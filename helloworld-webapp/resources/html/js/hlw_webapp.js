/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

function notification_button_onclick(event) {
  if (window.imports) {
    const Gio = imports.gi.Gio;
    var notification = Gio.Notification.new("Hello World!");
    var application = Gio.Application.get_default();
    application.send_notification("mynotification-hello", notification);
  } else {
    alert("Hello World!");
  }
}

window.onload = function (event) {
   document
     .getElementById("notification-button")
     .addEventListener("click", notification_button_onclick);
}
